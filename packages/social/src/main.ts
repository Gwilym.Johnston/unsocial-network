
import inquirer = require('inquirer');
// import { follow, post, read } from './app/actions';

const post = /(.+)( -> )(.+)/;



const questions = [
  {
    type: 'input',
    name: 'action',
    message: 'Welcome to the unsocial network, What do you want to do?',
    validate: async (input) => {
      console.log(post.exec(input));
      return true;
    }
  },
];


inquirer.prompt(questions).then((answers) => {
  console.log(JSON.stringify(answers, null, '  '));
});
