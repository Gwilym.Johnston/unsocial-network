
import { validateCommand } from './validateCommand';
describe('input validation', () => {
	test('command: alice -> this is a post', () => {
		const validationResult = validateCommand('alice -> this is a post');
		expect(validationResult).toBe({
			type: 'post',
			user: 'alice',
			post: 'this is a post'
		});
	})

})