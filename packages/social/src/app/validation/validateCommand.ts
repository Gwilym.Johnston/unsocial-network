interface Post {
  type: 'post',
  user: string,
  post: string
}

interface Read {
  type: 'read',
  user: string
}

interface Follow {
  type: 'follow',
  user: string,
  following: string
}


const isPost = /([a-zA-Z0-9]+)( -> ){1}(.+)/
const isRead = /^([a-zA-Z0-9]+){1}/
const isFollow = /([a-zA-Z0-9]+)( follows )([a-zA-Z0-9]+)/




export function validateCommand(input: string): Post | Read | Follow | null {
  if (isPost.test(input)) {
    const groups = isPost.exec(input);
    console.log(groups);
    return ({
      type: 'post',
      user: 'a',
      post: 'test'
    })
  }
  if (isFollow.test(input)) {
    return ({
      type: 'follow',
      user: 'a',
      following: 'b'
    })
  }
  if (isRead.test(input)) {
    return ({
      type: 'read',
      user: 'a',
    })
  }
}