
import { read } from './read';
describe('test read input', () => {
	test('a user can view another user\'s timeline', async () => {
		const result = await read();
		expect(result).toBe('Alice -> I love the weather today');
	})

})