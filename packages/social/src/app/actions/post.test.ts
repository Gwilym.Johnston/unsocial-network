
import { post } from './post';
describe('test post input', () => {
	test('a user can post a mesage', async () => {
		const result = await post();
		expect(result).toBe('Alice -> I love the weather today');
	})

})